# STEPS
https://www.youtube.com/watch?v=tiD2ju8dK24 --- Use this.
https://www.youtube.com/watch?v=3OkB9urqPP4&list=PLLMmnoVy03EegecY44FCmEIaLBUzJQ3W4&index=13

1. CD into the existing .yaml folder
2. Run -> cat docker-compose.yaml -> to print the code in the yaml file to the terminal
3. Run -> docker-compose -f docker-compose.yaml up -d --> or anydocker-compose-fileName.yaml
3. b. Or run -> docker compose up -d -> If you've explicitly named your file as 'docker-compose.yaml'
4. docker ps
5. Copy the id of the mongo db container and copy it in step 6 below
6. <!-- This will execute into the container that is running the database -->
   docker exec -it <docker-mongo-db-id> bash 
7. ls   
8. <!-- This successfully connects you to the database - Mongo Shell -->
mongo mongodb://localhost:27017 -u root -p password
   <!-- Then move to collections.md -->

   docker compose down -> To stop the containers
   docker compose up -d -> To start the containers in detached mode.





