# Documents - Update Existing Fields/Attributes
<!-- Orig name is Jane. So, you set to the name you want in the second argument.
Make sure you specify the id of the object to be updated, in the first argument -->
# Update Single Object
1. db.student.update({_id: ObjectId("626c87c205ea88d48016e7d6")}, {$set: {firstName: "Nyce"}});

# Update Many Objects
For all records where firstName is Beau update their country to Kenya.
2. db.student.updateMany({firstName: "Beau"}, {$set: {country: "Kenya"}})
3. Verify by running -> db.student.find({}).pretty(); //Returns all.

# Documents - Remove Some  Existing Fields/Attributes
<!-- For the student with the specified id, remove the gender attribute. -->
db.student.update({_id: ObjectId("626c87c205ea88d48016e7d5")}, {$unset: {gender: 1}});

# Increment Function
db.student.update({_id: ObjectId("626c826305ea88d48016e7d4")}, {$inc: {totalSpentInBooks: 2100}});
db.student.find({}, {totalSpentInBooks: 1});

# Pull Function - To remove a field from a student, say remove the favourite subjects
<!-- Find all students by their favourite subjects -->
1. db.student.find({}, {favouriteSubjects: 1});
<!-- Remove maths as a favourite subject for the student with this id -->
2. db.student.update({_id: ObjectId("626c87c205ea88d48016e7d6")}, {$pull: {favouriteSubjects: "maths"}});
<!-- Get all students again to verify that step 2 was successful -->
3. db.student.find({}, {favouriteSubjects: 1});

# Push Function - To add a field to a student, say add the favourite subjects
<!-- Add Art as a favourite subject of this student -->
db.student.update({_id: ObjectId("626c87c205ea88d48016e7d6")}, {$push: {favouriteSubjects: "art"}});
db.student.find({_id: ObjectId("626c826305ea88d48016e7d4")}, {favouriteSubjects: 1}).pretty(); 




