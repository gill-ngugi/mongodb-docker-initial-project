# COLLECTIONS
1. Show databases; OR Show dbs;
<!-- Create database or show if exists -->
2. use gillNewDb;
3. db.getName(); ...Displays name of created/referenced db; -> Returns gillNewDb; 

# Create Collection
db.createCollection("hello");
show dbs;

# Drop Database
db.dropDatabase();

# View all methods availabl7
db.help(); 
Documents = Rows
Collections = Tables

<!-- Creates the collection, if it doesnt exist yet, which equals a table -->
1. db.createCollection("person"); 
2. show collections;
<!-- View Collection Stats -->
3. db.person.stats();
<!-- Drop the collection -->
4. db.person.drop();
5. show collections;
6. db.createCollection("person", {capped: true, size: 6142800, max: 300});
7. db.person.stats();

