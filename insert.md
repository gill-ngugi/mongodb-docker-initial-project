# Documents - Insert Single Student
*** Adding a collection without using db.createCollection();
1. MongoDB stores data records as BSON documents. BSON is a binary representation of JSON objects.
2. You can use documents to create collections.
3. Copy and Paste the student object to the terminal and hit enter.
4. db.student.insert(student); 
OR 
   db.student.insert({});
5. db.student.count(); //Will return 1
6. show collections; //Will return student because it has been added

7. db.student.find(); // Returns the student object.
8. db.student.find().pretty(); //Returns the student object prettified.

# To Note
<!-- This inserts one document to student collection -->
db.student.insert(student); 
<!-- This inserts many documents to student collection -->
db.student.insertMany(students);

# Documents - Insert Many Students
1. Copy and Paste the students array to the terminal and hit enter.
2. db.student.insertMany(students);
OR
3. db.student.insertMany([{}, {}, {}]);
4. db.student.count(); 
OR
   db.student.find().pretty().count();



