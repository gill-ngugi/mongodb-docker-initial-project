student = {
    "firstName": "Beau",
    "email": "beau@hotmail.com",
    "gender": "F",
    "country": "Sao Tome",
    "isStudentActive": true,
    "favouriteSubjects": [
        "maths",
        "astronomy"
    ],
    "totalSpentInBooks": 10000 
}

students = [
    {
        "firstName": "Yttria",
        "email": "beau@hotmail.com",
        "gender": "F",
        "country": "Sao Tome",
        "isStudentActive": true,
        "favouriteSubjects": [
            "maths",
            "astronomy"
        ],
        "totalSpentInBooks": 10000  
    },
    {
        "firstName": "Nice",
        "email": "beau@hotmail.com",
        "gender": "F",
        "country": "Sao Tome",
        "isStudentActive": true,
        "favouriteSubjects": [
            "maths",
            "astronomy"
        ],
        "totalSpentInBooks": 10000 
    }
]