# Delete Single Student by id
db.student.count(); OR -> db.student.find({}).pretty().count();
db.student.deleteOne({_id: ObjectId("626c87c205ea88d48016e7d5")});
db.student.find({}).pretty();
db.student.count();

# Delete Single Student by another attribute
db.student.count();
db.student.deleteOne({firstName: "Nyce"});
db.student.count();

# Delete All Students with the email -> abc@123.com
db.student.count();
db.student.deleteMany({email: "abc@123.com"});
db.student.count();

# Delete All Students
db.student.deleteMany({});



