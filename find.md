# Steps
1. Find is similar to SELECT in SQL.

<!-- This returns all records -->
2. Select * FROM student; 
   == db.student.find({}).pretty(); OR db.student.find({}, {firstName: 1, lastName:1, gender:1).pretty();

3. Select * FROM student WHERE firstName="Beau";
   == db.student.find({firstName: "Beau"}).pretty();

4. SELECT firstName, lastName, gender FROM student WHERE firstName=Beau.
   db.student.find({firstName: "Beau"}, {firstName: 1, lastName:1, gender:1}).pretty();

5. IF YOU WANT TO RETURN ALL COLUMNS IN A TABLE WITH AN EXCEPTION OF SOME COLUMNS NOT BEING RETURNED:
   <!-- The below statement will return all other columns but those specified below -->
   db.student.find({firstName: "Beau"}, {firstName: 0, lastName: 0, gender: 0}).pretty();

